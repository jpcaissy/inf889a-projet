package main

import (
	"context"
	"fmt"
	"go/ast"
	"go/constant"
	"go/parser"
	"go/token"
	"go/types"
	"golang.org/x/tools/go/loader"
	"golang.org/x/tools/go/ssa"
	"golang.org/x/tools/go/ssa/ssautil"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"sync"

	"github.com/jpcaissy/gotcha/worklist"

	ssabuilder "github.com/nickng/dingo-hunter/ssabuilder"
	logrus "github.com/sirupsen/logrus"
)

var log *logrus.Logger

func init() {
	log = logrus.New()
	log.SetLevel(logrus.PanicLevel)
	log.SetOutput(os.Stdout)
	log.SetReportCaller(false)
}

type Analysis struct {
	loaderConfig *loader.Program
	parsedFile   *ast.File
	src          []byte
	fset         *token.FileSet
}
type AnalysisErrorType int

const (
	TaintError   = iota
	ParsingError = iota
)

type Node struct {
	Func     *ssa.Function
	Children []*Node
}

type Error struct {
	Type         AnalysisErrorType
	Pos          token.Position
	Explaination string
}

type CallGraph struct {
	program          *ssa.Program
	visitedFuncs     map[*ssa.Function]bool
	visitedBlocks    map[*ssa.BasicBlock]bool
	root             *Node
	nbNodesVisited   int
	nbBlocksVisited  int
	globals          map[string]string
	clientInstances  map[string][]string
	requestInstances map[string][]string
	requestHeaders   map[string]*http.Header
	requestVars      map[string][]string
	graphqlClients   map[string]*GraphqlClient
	requestQuery     map[string]string
}

func (cfg *CallGraph) Build(main *ssa.Function) {
	cfg.nbNodesVisited = 1
	cfg.nbBlocksVisited = 1
	cfg.root = &Node{
		Func:     main,
		Children: []*Node{},
	}
	cfg.visitedFuncs[cfg.root.Func] = true
	cfg.visitBlock(cfg.root.Func.Blocks[0], cfg.root)
}

func (cfg *CallGraph) visitBlock(block *ssa.BasicBlock, node *Node) {
	if _, visited := cfg.visitedBlocks[block]; visited {
		return
	}

	log.Printf("	Visiting block %d:%s\n", block.Index, block.Comment)
	cfg.visitedBlocks[block] = true
	cfg.nbBlocksVisited += 1
	for _, instruction := range block.Instrs {
		cfg.nbNodesVisited += 1
		switch instruction := instruction.(type) {
		case *ssa.Go:
			// if static callee is nil, it's a builtin func and we don't care
			if f := instruction.Common().StaticCallee(); f != nil {
				if _, visited := cfg.visitedFuncs[f]; !visited {
					cfg.visitedFuncs[f] = true
					node.Children = append(node.Children, &Node{
						Func:     f,
						Children: []*Node{},
					})
				}
			}

		case *ssa.Call:
			args := instruction.Common().Args
			var mode string
			if instruction.Common().Method == nil {
				mode = "call"
			} else {
				mode = "invoke"
			}

			if len(args) > 0 {
				callee := args[0]
				other_args := args[1:]
				log.Printf("		Call mode=%s, %+v = (%+v).(%s, %+v)\n", mode, instruction.Name(), instruction.Common().StaticCallee(), callee.Name(), other_args)
			} else {
				log.Printf("		Call mode=%s %+v = (%+v).(no args)\n", mode, instruction.Name(), instruction.Common().StaticCallee())
			}

			// ditto, if static callee is nil, it's a builtin func and we don't care
			if f := instruction.Common().StaticCallee(); f != nil {
				if _, visited := cfg.visitedFuncs[f]; !visited {
					cfg.visitedFuncs[f] = true
					node.Children = append(node.Children, &Node{
						Func:     f,
						Children: []*Node{},
					})
				}
			} else {
				log.Printf("			Not visiting call\n")
			}
			cfg.visitCall(instruction)

		case *ssa.Store:
			log.Printf("		Store on %+v\n", instruction)

		case *ssa.If:
			// visit both sides of if statement
			cfg.visitBlock(instruction.Block().Succs[0], node)
			cfg.visitBlock(instruction.Block().Succs[1], node)

		case *ssa.Jump:
			// end of a block,
			cfg.visitBlock(instruction.Block().Succs[0], node)

		case *ssa.Return:
			// end of a function, let's unwrap all unvisited children nodes
			for _, child := range node.Children {
				if _, visited := cfg.visitedFuncs[child.Func]; !visited {
					cfg.visitedFuncs[child.Func] = true
					cfg.visitBlock(child.Func.Blocks[0], child)
				}
			}

		case *ssa.UnOp:
			if instruction.Op == token.MUL {
				log.Printf("		UnOp for %+v=*%+v\n", instruction.Name(), instruction.X.Name())
				// TODO extract somewhere maybe?
				// TODO unefficiant
				// TODO what if key is the instruction.Name() or instruction.X.Name() ?
				log.Printf("			Checking request if %s is in %+v\n", instruction.X.Name(), cfg.requestInstances)
				for r, a := range cfg.requestInstances {
					for _, a1 := range a {
						if a1 == instruction.X.Name() {
							log.Printf("			Found %s in request Instances, referencing %s\n", a1, r)
							cfg.assignToRequests(r, instruction.Name())
						}
					}
				}
				log.Printf("			Checking instance if %s is in %+v\n", instruction.X.Name(), cfg.clientInstances)
				for r, a := range cfg.clientInstances {
					for _, a1 := range a {
						if a1 == instruction.X.Name() {
							log.Printf("			Found %s in request Instances, referencing %s\n", a1, r)
							cfg.assignToClients(r, instruction.Name())
						}
					}
				}

			} else {
				log.Printf("		UnOp for %s with unsupported type %+v\n", instruction.Name(), instruction.Op)
			}

		case *ssa.Alloc:
			log.Printf("		Alloc on %+v\n", instruction.Name())

		case *ssa.FieldAddr:
			cfg.assignToClients(instruction.X.Name(), instruction.Name())
			cfg.assignToRequests(instruction.X.Name(), instruction.Name())
			log.Printf("		FieldAddr %s=%+v (%+v)\n", instruction.Name(), instruction.X.Name(), instruction.X)

		case *ssa.DebugRef:
			continue
		default:
			log.Printf("		Uncaught instruction %T\n", instruction)
		}
	}
}

func (cfg *CallGraph) assignToRequests(left string, right string) {
	log.Printf(">>> requestInstances left=%s, right=%s, before=%v\n", left, right, cfg.requestInstances)
	if i, ok := cfg.requestInstances[left]; ok {
		log.Printf("	adding %s to %s\n", right, left)
		cfg.requestInstances[left] = append(i, right)
	}
	log.Printf(">>> requestInstances left=%s, right=%s, after %+v\n", left, right, cfg.requestInstances)
}

func (cfg *CallGraph) assignToClients(left string, right string) {
	log.Printf(">>> clientInstances left=%s, right=%s, before=%v\n", left, right, cfg.clientInstances)
	if i, ok := cfg.clientInstances[left]; ok {
		log.Printf("	adding %s to %s\n", right, left)
		cfg.clientInstances[left] = append(i, right)
	}
	log.Printf(">>> clientInstances left=%s, right=%s, after %+v\n", left, right, cfg.clientInstances)
}

func (cfg *CallGraph) visitCall(instruction *ssa.Call) {
	log.Printf("			Visiting call\n")
	call := instruction.Value().Common().Value.String()
	// TODO does not work on pointers
	// TODO checking string is the worse idea IMO, there ought to be better ways
	switch call {
	case "github.com/machinebox/graphql.NewClient":
		log.Printf("			Got graphql.NewClient call\n")
		endpoint := cfg.getValue(instruction.Common().Args[0])
		cfg.clientInstances[instruction.Name()] = append(cfg.clientInstances[instruction.Name()], instruction.Name())
		log.Printf("				Adding %s to clients: %+v\n", instruction.Name(), cfg.clientInstances)

		// TODO check if rqeuest already exists (but i doubt SSA allows it)
		cfg.graphqlClients[instruction.Name()] = NewGraphqlClient(endpoint)
		log.Printf("				Graphql client endpoint: %s\n", endpoint)
	case "github.com/machinebox/graphql.NewRequest":
		log.Printf("			Got graphql.NewRequest call for %s\n", instruction.Name())
		query := cfg.getValue(instruction.Common().Args[0])
		log.Printf("				Graphql request query len=%d\n", len(query))
		cfg.requestInstances[instruction.Name()] = append(cfg.requestInstances[instruction.Name()], instruction.Name())
		log.Printf("				Adding %s to requests: %+v\n", instruction.Name(), cfg.requestInstances)
		cfg.requestHeaders[instruction.Name()] = &http.Header{}
		cfg.requestQuery[instruction.Name()] = query
		log.Printf(">>> requestInstances initialized %+v\n", cfg.requestInstances)
	// TODO huh? really?
	case "(*github.com/machinebox/graphql.Request).Var":
		callee := instruction.Common().Args[0].Name()
		v := cfg.getValue(instruction.Common().Args[1])
		cfg.requestVars[callee] = append(cfg.requestVars[callee], v)
		log.Printf("			Call to  graphql.Request.Var for %+v with var %+v\n", callee, v)
	// TODO distinguish add and set
	case "(net/http.Header).Add":
	case "(net/http.Header).Set":
		callee := instruction.Common().Args[0].Name()
		log.Printf("			Call to http.Header.Set for %+v\n", callee)

		// DRY with .Do()
		requestInstance := ""
		for r, v := range cfg.requestInstances {
			if callee == r {
				requestInstance = r
				break
			}
			for _, a := range v {
				if a == callee {
					requestInstance = r
					break
				}
			}
		}
		// TODO this sucks
		if requestInstance != "" {
			key := cfg.getValue(instruction.Common().Args[1])
			value := cfg.getValue(instruction.Common().Args[2])
			if call == "(net/http.Header).Add" {
				cfg.requestHeaders[requestInstance].Add(key, value)
			} else {
				cfg.requestHeaders[requestInstance].Set(key, value)
			}
			log.Printf("			### Setting header to a graphql request (%s, %+v): %+v\n", key, value, cfg.requestHeaders[requestInstance])
		} else {
			log.Printf("			XXX cannot find request instance for %s in %+v\n", callee, cfg.requestInstances)
		}

	case "(*github.com/machinebox/graphql.Client).Run":
		callee := instruction.Common().Args[0].Name()
		graphqlRequest := instruction.Common().Args[2].Name()
		dataSet, ok := instruction.Common().Args[3].(*ssa.MakeInterface)
		if !ok {
			log.Fatal("Failed to cast to interface")
		}

		log.Printf("			Call to graphql.Client.Run for %+v with request %+v and data %+v\n", callee, graphqlRequest, dataSet.X.Name())

		// TODO this is a copy paste from set header
		clientInstance := ""
		for r, v := range cfg.clientInstances {
			if callee == r {
				clientInstance = r
				break
			}
			for _, a := range v {
				if a == callee {
					clientInstance = r
					break
				}
			}
		}

		requestInstance := ""
		for r, v := range cfg.requestInstances {
			if graphqlRequest == r {
				requestInstance = r
				break
			}
			for _, a := range v {
				if a == graphqlRequest {
					requestInstance = r
					break
				}
			}
		}
		if clientInstance != "" && requestInstance != "" {
			log.Printf("			### Found Run() call to graphql client %s with request %s and vars %+v\n", clientInstance, requestInstance, cfg.requestVars[requestInstance])
			g, ok := cfg.graphqlClients[clientInstance]
			if ok {
				g.Build(cfg.requestHeaders[requestInstance], cfg.requestQuery[requestInstance], cfg.requestVars[requestInstance], instruction.Pos())
			} else {
				log.Printf("			XXXXXX cannot find graphql client %s\n", clientInstance)
			}
		} else {
			log.Printf("			### Found Run() call to unsupported client %s for request %s\n", callee, graphqlRequest)
		}

	default:
		log.Printf("			Unknown call to %s\n", call)
	}
}

func (cfg *CallGraph) getValue(arg ssa.Value) string {
	if c, ok := arg.(*ssa.Const); ok {
		if t, ok := c.Type().Underlying().(*types.Basic); ok {
			switch t.Kind() {
			// TODO other const values https://github.com/golang/tools/blob/213b5e130a7c37d6bcca5383d4e2ea08250014b5/go/ssa/interp/ops.go
			case types.String, types.UntypedString:
				if c.Value.Kind() == constant.String {
					return constant.StringVal(c.Value)
				}
				// TODO is this good?
				return string(rune(c.Int64()))
			}
		}
	} else if c, ok := arg.(*ssa.UnOp); ok {
		if c.Op == token.MUL {
			log.Printf("			got value referenced %T:%+v\n", c, c)
			if g, ok := c.X.(*ssa.Global); ok {
				o := g.Object()
				log.Printf("			got global object named %T:%+v\n", o.Name(), o.Name())
				if v, ok := cfg.globals[o.Name()]; ok {
					return v
				}
				// TODO error management
			}
		}
	}

	// TODO error management
	return ""
}

func (c *CallGraph) RunGraphqlAnalysis(analysis *Analysis, wg *sync.WaitGroup, ch chan Error) {
	defer wg.Done()

	log.Printf(">>>>>>>>	Running checker (len=%d), %+v\n", len(c.graphqlClients), c.graphqlClients)
	for i, client := range c.graphqlClients {
		log.Printf("	Got client %s\n", i)
		if !client.built {
			log.Println("		Skipping because not built")
		} else {
			log.Printf("	Running introspection query for %+v\n", client)
			for _, e := range client.Introspect() {
				ch <- Error{
					Type:         ParsingError,
					Pos:          analysis.fset.Position(client.Pos),
					Explaination: e.Message,
				}
			}
		}
	}
	log.Println("<<<<<<<<	Checker ran")
}

func (a *Analysis) RunTaintChecker(wg *sync.WaitGroup, ch chan Error) {
	defer wg.Done()

	log.Printf(">>>>>>>> taint analysis\n")

	err := worklist.DoAnalysis("github.com/jpcaissy/gotcha", []string{
		os.Args[1],
	}, "./sqlInjection.txt", false, "", true)

	if err == nil {
		log.Println("	Hourra, no taints")
	} else {
		log.Printf("	##### Errors in taint analysis!\n")
		switch err := err.(type) {
		case *worklist.ErrInFlows:
			log.Printf("		err.NumberOfFlows: %d \n", err.NumberOfFlows())
			log.Printf("		number of errors: %d\n", len(err.Errors()))
			for i, e := range err.Errors() {
				ch <- Error{
					Type:         TaintError,
					Pos:          a.fset.Position(e.Call.Pos()),
					Explaination: "The `Query` method is using a tainted value",
				}
				log.Printf("			#%d: pos:%+v\n", i, a.fset.Position(e.Call.Pos()))
			}
		default:
			log.Fatal(err)
		}
	}
	log.Printf("<<<<<<<< taint analysis\n")

}

func (a *Analysis) BuildCallGraph() *CallGraph {
	log.Println(">>>>>>>>	SSA")
	prog := ssautil.CreateProgram(a.loaderConfig, ssa.GlobalDebug|ssa.BareInits)
	// TODO check for bad packages
	prog.Build()

	// get main package
	var mainPkg *ssa.Package
	pkgs := prog.AllPackages()
	for _, pkg := range pkgs {
		if pkg.Pkg.Name() == "main" {
			mainPkg = pkg
			break
		}
	}

	if mainPkg == nil {
		log.Fatal("Did not find main package")
	}

	// getting global declrations
	// TODO ugly, but works.
	// TODO we can also not get strings,
	// look at https://github.com/lukehoban/go-outline/blob/e785568742524aa488d7332e7619dba87b2a8b9d/main.go#L54-L107
	// or https://gist.github.com/ncdc/fef1099f54a655f8fb11daf86f7868b8
	log.Printf("Package name: %s\n", a.parsedFile.Name.Name)
	globals := make(map[string]string)
	for i, d := range a.parsedFile.Decls {
		log.Printf("#%d %T:%+v\n", i, d, d)
		if genericDecl, ok := d.(*ast.GenDecl); ok {
			// We only want var or const
			//if genericDecl.Tok == token.CONST || genericDecl.Tok == token.VAR {
			// Capture if there are multiple
			for i2, s := range genericDecl.Specs {
				log.Printf("#%d,%d Decl: %+v\n", i, i2, s)
				if vSpec, ok := s.(*ast.ValueSpec); ok {
					log.Printf("#%d,%d ValueSpec: %+v\n", i, i2, vSpec)
					for i := 0; i < len(vSpec.Names); i++ {
						// TODO: only basic literals work currently
						switch v := vSpec.Values[i].(type) {
						case *ast.BasicLit:
							unquotedString, err := strconv.Unquote(v.Value)
							log.Printf("++++ Got global declaration of %s\n", vSpec.Names[i].Name)
							if err == nil {
								globals[vSpec.Names[i].Name] = unquotedString
							} else {
								log.Printf("++++ Failed to unquote value=%+v\n", err)
							}

						default:
							log.Printf("Name: %s - Unsupported ValueSpec: %+v\n", vSpec.Names[i].Name, v)
						}
					}
				}
			}
			//}
		}
	}
	log.Printf("BUilt globals with %T:%+v\n", globals, globals)

	log.Println("Building CFG")
	callGraph := &CallGraph{
		visitedFuncs:     make(map[*ssa.Function]bool),
		visitedBlocks:    make(map[*ssa.BasicBlock]bool),
		requestInstances: make(map[string][]string),
		clientInstances:  make(map[string][]string),
		program:          prog,
		globals:          globals,
		requestHeaders:   make(map[string]*http.Header),
		graphqlClients:   make(map[string]*GraphqlClient),
		requestVars:      make(map[string][]string),
		requestQuery:     make(map[string]string),
	}
	callGraph.Build(mainPkg.Func("main"))
	log.Printf("CFG built nodes=%d blocks=%d\n", callGraph.nbNodesVisited, callGraph.nbBlocksVisited)

	return callGraph
}

func (a *Analysis) WriteSsa(wg *sync.WaitGroup) {
	defer wg.Done()

	log.Println("Writing SSA to file")
	ssa, err := ssabuilder.NewConfigFromString(string(a.src))
	ssaInfo, err := ssa.Build()
	if err != nil {
		log.Fatal(err)
	}

	f2, err := os.OpenFile("ssa", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	_, err = ssaInfo.WriteTo(f2)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("<<<<<<<<	SSA")
}

func (a *Analysis) ReadErrors(errors *[]Error, wg *sync.WaitGroup, ctx context.Context, ch chan Error) {
	defer wg.Done()

	for {
		select {
		case <-ctx.Done():
			return
		case e := <-ch:
			log.Printf("Got error\n")
			*errors = append(*errors, e)
		}
	}
}

func main() {
	log.WithFields(logrus.Fields{
		"fileName": os.Args[1],
	}).Debug("Reading file")
	src, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	fset := token.NewFileSet()
	parsedFile, err := parser.ParseFile(fset, os.Args[1], src, 0)
	if err != nil {
		log.Fatal(err)
	}

	// use own loader config, this is just necessary
	var config loader.Config
	// TODO main is package name, but it isn't taken from file
	f, err := config.ParseFile("main", src)
	if err != nil {
		log.Fatal(err)
	}
	// TODO main is package name, but it isn't taken from file
	config.CreateFromFiles("main", f)
	loaderConfig, err := config.Load()
	if err != nil {
		log.Fatal(err)
	}

	errors := []Error{}
	ch := make(chan Error, 1000)
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	errorWg := &sync.WaitGroup{}

	analysis := &Analysis{
		loaderConfig: loaderConfig,
		parsedFile:   parsedFile,
		src:          src,
		fset:         fset,
	}

	errorWg.Add(1)
	go analysis.ReadErrors(&errors, errorWg, ctx, ch)

	wg.Add(2)
	go analysis.WriteSsa(wg)
	go analysis.RunTaintChecker(wg, ch)

	callGraph := analysis.BuildCallGraph()

	wg.Add(1)
	go callGraph.RunGraphqlAnalysis(analysis, wg, ch)

	wg.Wait()
	cancel()
	errorWg.Wait()

	if len(errors) > 0 {
		//fmt.Printf("Found %d error(s):\n", len(errors))
		for _, e := range errors {
			var t string
			if e.Type == TaintError {
				t = "Taint analysis"
			} else {
				t = "GraphQL parsing analysis"
			}
			//fmt.Printf("	%s error:\n", t)
			fmt.Printf("%s: (%s) %s\n", e.Pos, t, e.Explaination)
		}
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}

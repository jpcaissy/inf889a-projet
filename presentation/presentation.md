% INF889A - Présentation du projet de session
% Jean-Philippe Caissy
% 22 avril 2020

---
header-includes:
 - \usepackage{xmpmulti}
 - \usepackage[canadien]{babel}
 - \usepackage{animate}
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---

# Projet

* Outil d'analyseur statique d'applications Go pour GraphQL

[https://gitlab.com/jpcaissy/inf889a-projet](gitlab.com/jpcaissy/inf889a-projet)

# Projet
## GraphQL

GraphQL est un langage de requête pour API web[^a]. Dévelopé initialement par Facebook.

Avantages (et différences de REST) :

* Permet au client de définir la structure des données à recevoir
* Plusieurs ressources par requête
* Système de type
* Introspection du schéma à partir du client

[^a]: [https://graphql.org/](https://graphql.org/)

# Projet
## GraphQL

::: columns

:::: column
### Requête 

```graphql
{
    hero {
        name
        friends {
            name
        }
    }
}
```

::::

:::: column
### Réponse 
```json
{
  "hero": {
    "name": "Luke Skywalker",
    "friends": [
        { "name": "Obi-Wan Kenobi" },
        { "name": "R2-D2" },
        { "name": "Han Solo" }
    ]
  }
}
```
::::

:::

# Projet
## GraphQL

::: columns

:::: column

```
type Query {
  hero: Character
}

type Film {
  title: String
  episode: Int
  releaseDate: String
}
```
::::

:::: column
```
type Character {
  name: String
  friends: [Character]
  homeWorld: Planet
  species: Species
}

type Planet {
  name: String
  climate: String
}
```
::::
:::

* Exemples de types : types primitifs (int, str, etc), objets, listes, enum, alias de types, etc

# Projet
## GraphQL

Un client peut demander l'introspection[^d] du schéma complet d'un serveur

* Objets
* Types
* Directives et mutations (pour modifier les données)

[^d]: [https://graphql.org/learn/introspection/](https://graphql.org/learn/introspection/)

# Projet
## Go

Avantages :

* Langage avec une vérification de type statique
* Parsing d'AST inclus dans la *stdlib*[^b]
* Interprétation intermédiaire en SSA[^c]
    * Simplifie énormément l'analyse statique

[^b]: [https://pkg.go.dev/go/ast](https://pkg.go.dev/go/ast?tab=doc)
[^c]: [https://pkg.go.dev/golang.org/x/tools/go/ssa](https://pkg.go.dev/golang.org/x/tools/go/ssa)

# Projet
## Objectif

* `[ ]`Analyse statique de code source Go utilisant GraphQL
* `[ ]`Détecter l'utilisation d'une librairie client (graphql-qo/graphql)
* `[ ]`Récupérer le schéma GraphQL distant
* `[ ]`Valider les types de l'application
* `[ ]`Analyse de variables vivantes

# Projet
## La réalité d'un ~~étudiant~~ projet de session

* `[X]` Analyse statique de code source Go utilisant GraphQL
* `[X]` Détecter l'utilisation d'une librairie client (graphql-qo/graphql)
* `[X]` Récupérer le schéma GraphQL distant
* `[ ]` ~~Valider les types de l'application~~
    * `[x]` Valider les requêtes statiques de l'application *(ish)*
* `[ ]` ~~Analyse de variables vivantes~~
    * `[x]` Intégration d'un taint analysis pour les requêtes *(ish)*
* `[X]` Intégration dans Vim avec Syntastic

# Projet
## Fonctionnement

1. Parsing de l'AST pour récupérer les variables globales (e.g.: constantes)
2. Construction du code intermédiaire en SSA
3. Interprétation du SSA pour récupérer les jetons de l'AST :
    1. Instanciation des clients GraphQL : `graphql.NewClient(URL)`
    2. Instanciation des requêtes: `graphql.NewRequset(REQUEST)`
    3. Récupération des méta-données (*headers* HTTP et variables)
4. Récupération du schéma distant à partir des clients (3.1)
    1. Parsing du schéma distant et validation des requêtes (3.2)
5. Lancement d'un *taint analysis* pour les méta-données (3.3)

# Projet
## Limitations

* Ne supporte pas les *packages* Go (doit parser un fichier à la fois)
* L'interprétation du SSA est limité et de base
* Le *taint analysis* fait du dataflow, mais il est facile de l'induire en erreur
    * La définition des *sinks* et *sources* est statique et dans un format custom

# Projet
## Librairies tierces utilisées
### Serveur GraphQL

* `jpcaissy/graphql-go` (fork de graph-gophers/graphql-go[^i])


    1. *Packages* de validation privés
    2. Objet interne `schema` privé

### Taint Analysis

Utilise un cadre monotone pour faire du *taint analysis* avec une analyse de flôt (*information flow analysis*).
Plusieurs références aux danois.

* `jpcaissy/gotcha` (fork de akwick/gotcha[^e])

    1. Suppression de logs de debug inutiles
    2. `Invalid memory address`, aka null pointer

[^i]: [https://github.com/graph-gophers/graphql-go/compare/master...jpcaissy:master](https://github.com/graph-gophers/graphql-go/compare/master...jpcaissy:master)
[^e]: [https://github.com/akwick/gotcha/compare/master...jpcaissy:master](https://github.com/akwick/gotcha/compare/master...jpcaissy:master)

# Projet
## Librairies tierces utilisées
### Implémentation de référence de GraphQL

`graphql/graphql-js`[^f]

Afin de convertir l'introspection du schéma distant vers un format supporté par le serveur GraphQL.[^g]

[^f]: [https://github.com/graphql/graphql-js](https://github.com/graphql/graphql-js)
[^g]: [https://gitlab.com/jpcaissy/inf889a-projet/-/blob/master/schema_buidler/builder.js](https://gitlab.com/jpcaissy/inf889a-projet/-/blob/master/schema_buidler/builder.js)

# Projet
## Rétrospective

* Avoir un plan (explorer les autres outils existants)
    * e.g.: analyses dataflow déjà existantes[^h]
* Interpréter un AST à partir de zéro c'est diffile!
* Écrire un analyseur statique c'est encore plus difficile!!
* L'interprétation intermédiaire (SSA) a de gros avantages
* Vimscript c'est pas fameux

## Travail ultérieur

* Analyses de types
* Possibilités d'intégratins à un CI
* Ré-utilisation de l'interprétation du SSA

[^h]: [https://github.com/godoctor/godoctor/tree/master/analysis/dataflow](https://github.com/godoctor/godoctor/tree/master/analysis/dataflow)

# Projet
## Démo

#!/usr/bin/env nodejs

const graphql = require("graphql");
var fs = require("fs");

var stdinBuffer = fs.readFileSync(0); // STDIN_FILENO = 0
var schema = JSON.parse(stdinBuffer.toString());

const clientSchema = graphql.buildClientSchema(schema.data);
const schemaString = graphql.printSchema(clientSchema);

process.stdout.once('drain', function () { process.exit(0); });
process.stdout.write(schemaString)

if exists('g:loaded_syntastic_go_gographql_checker')
    finish
endif
let g:loaded_syntastic_go_gographql_checker = 1

let s:save_cpo = &cpo
set cpo&vim

function! SyntaxCheckers_go_gographql_GetLocList() dict
    let buf = bufnr('')
    let makeprg = self.makeprgBuild({})

    let errorformat = '%f:%l:%c: %m,'

    return SyntasticMake({
        \ 'makeprg': makeprg,
        \ 'fname': bufname(buf),
        \ 'errorformat': errorformat,
        \ 'defaults': {'type': 'w'},
        \ 'subtype': 'Style' })
endfunction

call g:SyntasticRegistry.CreateAndRegisterChecker({
    \ 'filetype': 'go',
    \ 'exec': '/home/jpcaissy/src/inf889a-projet/main',
    \ 'name': 'gographql'})

let &cpo = s:save_cpo
unlet s:save_cpo


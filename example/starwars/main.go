package main

import (
	"net/http"

	"github.com/graphql-go/handler"
	"github.com/graphql-go/graphql/testutil"
)

func main() {
	schema := testutil.StarWarsSchema
	h := handler.New(&handler.Config{
		Schema: &schema,
		Pretty: true,
		Playground: true,
	})

	http.Handle("/graphql", h)
	http.ListenAndServe(":8080", nil)
}

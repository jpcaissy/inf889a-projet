package main

import (
	"context"
	"fmt"
	"net/http"
	"io/ioutil"

	my_graphql "github.com/machinebox/graphql"
	log "github.com/sirupsen/logrus"
)

var ExampleQuery = `
query ($query: String!)
{
  products(first: 10, query:$query) {
    edges {
      node {
        id,
        title,
        description,
          onlineStoreUrl,
          options {
            name,
            values
        }
      }
    }
  }
}

`

type ExampleQueryStruct struct {
	Products struct {
		Edges []struct {
			Node struct {
				Id             string
				Title          string
				Description    string
				OnlineStoreUrl string
				Options        []struct {
					Name   string
					Values []string
				}
			}
		}
	}
}

//const GITHUB_ENDPOINT = "https://api.github.com/graphql"
const SHOPIFY_ENDPOINT = "https://graphql.myshopify.com" // /api/2020-01/graphql.json
const STARWARS_ENDPOINT = "http://localhost:8080"

func init() {
	log.SetLevel(log.WarnLevel)
}

func main() {
	var my_client_2 *my_graphql.Client
	var respData ExampleQueryStruct

	client := &http.Client{}
	//query := "updated_at:>2016-12-01"

	my_client := my_graphql.NewClient(SHOPIFY_ENDPOINT + "/api/2020-01/graphql.json")
	my_client2 := my_graphql.NewClient(STARWARS_ENDPOINT + "/graphql")
	//my_client.Log = func(l string) { log.Debug(l) }
	req := my_graphql.NewRequest(ExampleQuery)
	req2 := my_graphql.NewRequest(`
query {
  human(id: "1000") {
    name
	test
  }
}`)
	//q := os.Environ()[2]
	if false {
		//req.Var("query", q)
	}
	//req.Var("query", q)
	req.Var("query2", "foobar")

	//req.Header.Set("Authorization", "bearer "+os.Getenv("GITHUB_API_KEY"))
	req.Header.Set("X-Shopify-Storefront-Access-Token", "ecdc7f91ed0970e733268535c828fbbe")
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0")

	ctx := context.Background()
	if err := my_client.Run(ctx, req, &respData); err != nil {
		log.Fatal(err)
	}

	var dumper interface{}

	if err := my_client2.Run(ctx, req2, &dumper); err != nil {
		log.Fatal(err)
	}

	http_req, _ := http.NewRequest("GET", "http://example.com", nil)
	header := &http_req.Header
	header.Set("If-None-Match", `W/"wyzzy"`)
	http_resp, err := client.Do(http_req)
	if err != nil{
		log.Fatal(err)
	}

	defer http_resp.Body.Close()
	body, _ := ioutil.ReadAll(http_resp.Body)
	fmt.Printf("%s\n", body)


	for i := 0; i < 10; i++ {
		fmt.Printf(respData.Products.Edges[0].Node.Id)
	}

	my_client_2 = my_graphql.NewClient(SHOPIFY_ENDPOINT)
	fmt.Printf("%+v\n", respData)
	fmt.Printf("%+v\n", my_client_2)
	// log.Info(respData.Schema.Types[10])
}

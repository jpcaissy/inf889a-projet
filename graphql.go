package main

import (
	"context"
	"encoding/json"
	"go/token"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"

	"github.com/jpcaissy/graphql-go/errors"
	"github.com/jpcaissy/graphql-go/external/query"
	"github.com/jpcaissy/graphql-go/external/schema"
	"github.com/jpcaissy/graphql-go/external/validation"

	graphql_client "github.com/machinebox/graphql"
)

const OBJECT = "OBJECT"

var IntrospectionQuery = `
query IntrospectionQuery {
  __schema {
    queryType {
      name
    }
    mutationType {
      name
    }
    subscriptionType {
      name
    }
    types {
      ...FullType
    }
    directives {
      name
      description
      locations
      args {
        ...InputValue
      }
    }
  }
}

fragment FullType on __Type {
  kind
  name
  description
  fields(includeDeprecated: true) {
    name
    description
    args {
      ...InputValue
    }
    type {
      ...TypeRef
    }
    isDeprecated
    deprecationReason
  }
  inputFields {
    ...InputValue
  }
  interfaces {
    ...TypeRef
  }
  enumValues(includeDeprecated: true) {
    name
    description
    isDeprecated
    deprecationReason
  }
  possibleTypes {
    ...TypeRef
  }
}

fragment InputValue on __InputValue {
  name
  description
  type {
    ...TypeRef
  }
  defaultValue
}

fragment TypeRef on __Type {
  kind
  name
  ofType {
    kind
    name
    ofType {
      kind
      name
      ofType {
        kind
        name
        ofType {
          kind
          name
          ofType {
            kind
            name
            ofType {
              kind
              name
              ofType {
                kind
                name
              }
            }
          }
        }
      }
    }
  }
}
`

type GraphqlClient struct {
	endpoint string
	headers  *http.Header
	built    bool
	queries  []string
	vars     []string
	Pos      token.Pos
}

func NewGraphqlClient(endpoint string) *GraphqlClient {
	return &GraphqlClient{
		built:    false,
		endpoint: endpoint,
	}
}

func (g *GraphqlClient) Build(headers *http.Header, query string, vars []string, pos token.Pos) {
	g.headers = headers
	g.queries = append(g.queries, query)
	g.built = true
	g.Pos = pos

	for _, v := range vars {
		g.vars = append(g.vars, v)
	}

	log.Printf("***** Building graphql client for endpoint %s with query len=%d and vars=%+v\n", g.endpoint, len(query), vars)
}

type Wrapper struct {
	Data interface{} `json:"data"`
}

func (g *GraphqlClient) Introspect() []*errors.QueryError {
	ctx := context.Background()
	client := graphql_client.NewClient(g.endpoint)

	request := graphql_client.NewRequest(IntrospectionQuery)
	request.Header = g.headers.Clone()
	log.Printf("Headers: %+v\n", request.Header)
	log.Printf("Endpoint: %+v\n", g.endpoint)

	var data interface{}

	if err := client.Run(ctx, request, &data); err != nil {
		log.Println(err)
		return nil
	}

	cmd := exec.Command("./schema_buidler/builder.js")
	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}
	stdout, err := cmd.StdoutPipe()
	defer stdout.Close()

	if err != nil {
		log.Fatal(err)
	}
	cmd.Stderr = os.Stderr

	log.Println("launching command")
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	jsonData, err := json.Marshal(Wrapper{Data: data})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("writing to stdin len=%d\n", len(jsonData))

	n, err := stdin.Write(jsonData)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("wrote %d bytes to stdin\n", n)
	stdin.Close()

	output, err := ioutil.ReadAll(stdout)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("read %d bytes\n", len(output))

	log.Println("waiting to end")
	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}

	log.Println("finished, parsing schema")
	s := schema.New()
	if err := s.Parse(string(output), true); err != nil {
		log.Debug(err)
	}
	log.Println("Schema parsed")

	var errors []*errors.QueryError
	for _, q := range g.queries {
		parsedQuery, nerr := query.Parse(q)
		if nerr != nil {
			log.Println("Got error while parsing query")
			errors = append(errors, nerr)
		}
		log.Println("Validating query with schema")
		vars := make(map[string]interface{})
		for _, v := range g.vars {
			vars[v] = "toto"
		}
		log.Printf("Query: %s, vars:%+v\n", q, vars)
		for _, e := range validation.Validate(s, parsedQuery, vars, 10) {
			errors = append(errors, e)
		}
	}

	return errors
}

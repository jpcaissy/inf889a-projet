module projet-session

go 1.13

require (
	github.com/go-delve/delve v1.4.0 // indirect
	github.com/godoctor/godoctor v0.0.0-20181123222458-69df17f3a6f6
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jpcaissy/gotcha v0.0.0-00010101000000-000000000000
	github.com/jpcaissy/graphql-go v0.0.0-00010101000000-000000000000
	github.com/kr/pretty v0.1.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.2.0 // indirect
	github.com/nickng/dingo-hunter v1.0.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/willf/bitset v1.1.10 // indirect
	golang.org/x/tools v0.0.0-20200420001825-978e26b7c37c
)

replace github.com/jpcaissy/gotcha => /home/jpcaissy/src/gotcha/

replace github.com/jpcaissy/graphql-go => /home/jpcaissy/src/graphql-go/
